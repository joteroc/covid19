<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use \Carbon\Carbon;

class SolicitudBeneficiarioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitudes = App\SolicitudBeneficiario::All();
        return view ('solicitudbeneficiarios',compact('solicitudes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solicitudBeneficiario = new App\SolicitudBeneficiario;
        $solicitudBeneficiario->name = $request->name;
        $solicitudBeneficiario->lastname = $request->lastname;
        $solicitudBeneficiario->email = $request->email;
        $solicitudBeneficiario->estado = $request->estado;
        $solicitudBeneficiario->idType = $request->idType;
        $solicitudBeneficiario->idCode = $request->idCode;
        $solicitudBeneficiario->gender = $request->gender;
        $solicitudBeneficiario->comentario = $request->comentario;

        try{
            $date = Carbon::now();
            $solicitudesDiarias = App\SolicitudBeneficiario::Where('idCode',$request->idCode)->
                                                        whereDate('created_at','=', $date)->get();

        $solicitudesHoy=$solicitudesDiarias->count();
        //return $solicitudesHoy;

        $configMaxSolicitudes = App\Config::where('nombre',"limite_solicitudes")->get();
        //  return (int)$configMaxSolicitudes[0]->valor;
        if($solicitudesHoy>=(int)$configMaxSolicitudes[0]->valor){
            return back()->with('mensaje_error','Te pasaste :D, Maximo de Solicitudes diarias Alcanzado!');
        }else{
            $solicitudBeneficiario->save();
            return back()->with('mensaje','Agregado Correctamente');
        }

        }catch(\Illuminate\Database\QueryException $qe){
            return back()->with('mensaje_error','Ya tiene una solicitud vigente');
        }

        //return $request->All();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$solicitud = App\SolicitudBeneficiario::where('id', $id)->count();
    }

    public function showByIdBeneficiario($idCode)
    {

        //return $idCode;
        $solicitudes = App\SolicitudBeneficiario::Where('idCode',$idCode)->get();

        $solicitudesHoy = $solicitudes->count();


        if($solicitudesHoy==0){
            $solicitudes=null;
        }

        return view('misolicitud',compact('solicitudes'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function decidir(Request $request)
    {
        //return $request->All();
        $solicitud = App\SolicitudBeneficiario::find($request->id);
        $num_beneficiarios = 0;
        if($request->accion == 'aprobar'){
            $solicitud->estado = 'Aprobado';
            $solicitud->comentario = 'Su solicitud ha sido APROBADA';

            $beneficiario = App\Beneficiario::Where('idCode',$request->idCode)->get();
            $num_beneficiarios = $beneficiario->count();
        }
        else if($request->accion == 'denegar'){
            $solicitud->estado = 'Rechazado';
            $solicitud->comentario = 'Su solicitud ha sido RECHAZADA';
        }
        try{
            //Aqui se llena la tabla de beneficiarios si nunca ha sido aceptado
            if($num_beneficiarios>=0){
                //return $request->All();
                $usuario = App\User::Where('idCode',$request->idCode)->get();
                $numUsuarios = $usuario->count();
                if($numUsuarios>0)
                {
                    $beneficiarioNuevo = new App\Beneficiario();
                    $beneficiarioNuevo->name = $usuario[0]->name;
                    $beneficiarioNuevo->lastname = $usuario[0]->lastname;
                    $beneficiarioNuevo->idCode = $usuario[0]->idCode;
                    $beneficiarioNuevo->idType = $usuario[0]->idType;
                    $beneficiarioNuevo->save();
                }

            }
            $solicitud->save();
            return back()->with('mensaje','Modificacion Exitosa');
        }catch(\Illuminate\Database\QueryException $qe){
            return back()->with('mensaje_error','Ya existe una cuenta con el mismo correo o documento de identidad');
        }
    }
}
