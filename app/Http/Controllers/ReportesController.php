<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\UserExport;
use App\Exports\BeneficiarioExport;
use App\Exports\EnfermedadExport;
use App\Exports\RolExport;
use App\Exports\ConfigExport;
use App\Exports\SolicitudBeneficiarioExport;
use Maatwebsite\Excel\Facades\Excel;

class ReportesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function principal()
    {
        return view('reportes.principal');
    }

    public function usuarios(){
        $users = DB::select('select gender as genero, count(*) as Usuarios from users group by gender');
        /*
        $array[]=['Genero','Cantidad'];
        foreach($users as $user =>$value){
            $array[++$user]=[$value->genero,$value->usuarios];
        }*/
        //return view('reportes.usuarios')->with('genero',json_encode($array));
        //return view('reportes.usuarios',compact('users'));
        return Excel::download(new UserExport,'Listado_Usuarios.xlsx');

        /*
        select gender , count(*) as usuarios from users group by gender
        */
    }

    public function beneficiarios(){
        return Excel::download(new BeneficiarioExport,'Listado_Beneficiarios.xlsx');
    }

    public function enfermedades(){
        return Excel::download(new EnfermedadExport,'Listado_Enfermedades.xlsx');
    }

    public function roles(){
        return Excel::download(new RolExport,'Listado_Roles.xlsx');
    }

    public function configs(){
        return Excel::download(new ConfigExport,'Listado_Parametros_Sistema.xlsx');
    }

    public function solicitudes_beneficiarios(){
        return Excel::download(new SolicitudBeneficiarioExport,'Listado_Solicitudes.xlsx');
    }

    public function graficos_usuarios(){
        $users = DB::select('select gender as genero, count(*) as usuarios from users group by gender');

        $array[]=['Genero','Cantidad'];
        foreach($users as $key =>$value){
            $array[++$key]=[$value->genero,$value->usuarios];
        }
        //return json_encode($array);
        return view('reportes.usuarios', compact('array'))->with('genero',json_encode($array));
    }

    public function graficos_solicitudes(){
        $solicitudes = DB::select('select estado as estado, count(*) as cantidad from solicitud_beneficiarios group by estado');

        $array[]=['Estado','Cantidad'];
        foreach($solicitudes as $key =>$value){
            $array[++$key]=[$value->estado,$value->cantidad];
        }
        //return json_encode($array);
        return view('reportes.solicitudes', compact('array'))->with('solicitud',json_encode($array));
    }
}
