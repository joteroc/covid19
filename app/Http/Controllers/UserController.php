<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'lastname'=> ['required', 'string'],
            'idType'=> ['string'],
            'idCode'=> ['required', 'string'],
            'gender'=> ['required','string'],
            'rol'=> ['string'],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = App\User::All();
        return view ('usuarios',compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = App\User::findOrFail($id);
        return view ('perfil',compact('usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $user = App\User::find($request->id);
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->gender = $request->gender;
        $user->rol = $request->rol;
        $user->idCode = $request->idCode;
        $user->idType = $request->idType;
        $user->email = $request->email;

        try{
            $user->save();
            return back()->with('mensaje','Modificacion Exitosa');
        }catch(\Illuminate\Database\QueryException $qe){
            return back()->with('mensaje_error','Ya existe una cuenta con el mismo correo o documento de identidad');
        }
    }

    public function updatePassword(Request $request){

        $user = App\User::find($request->id);
        $user->password = Hash::make($request->password);

        try{
            $user->save();
            return back()->with('mensaje','Contraseña Modificada');
        }catch(\Illuminate\Database\QueryException $qe){
            return back()->with('mensaje_error','Hubo un error al cambiar la contraseña');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
