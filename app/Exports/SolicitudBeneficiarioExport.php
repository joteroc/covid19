<?php

namespace App\Exports;

use App\SolicitudBeneficiario;
use Maatwebsite\Excel\Concerns\FromCollection;

class SolicitudBeneficiarioExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return SolicitudBeneficiario::all();
    }
}
