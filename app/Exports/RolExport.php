<?php

namespace App\Exports;

use App\Rol;
use Maatwebsite\Excel\Concerns\FromCollection;

class RolExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Rol::all();
    }
}
