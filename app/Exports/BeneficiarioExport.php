<?php

namespace App\Exports;

use App\Beneficiario;
use Maatwebsite\Excel\Concerns\FromCollection;

class BeneficiarioExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Beneficiario::all();
    }
}
