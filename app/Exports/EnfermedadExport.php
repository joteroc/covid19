<?php

namespace App\Exports;

use App\Enfermedad;
use Maatwebsite\Excel\Concerns\FromCollection;

class EnfermedadExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Enfermedad::all();
    }
}
