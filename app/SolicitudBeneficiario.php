<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudBeneficiario extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','lastname','estado','idType','idCode','comentario','id'
    ];

}
