<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SintomaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sintomas')->insert([
            'nombre' => 'Fiebre',
            'descripcion' => Str::random(70),
        ]);

        DB::table('sintomas')->insert([
            'nombre' => 'Diarrea',
            'descripcion' => Str::random(70),
        ]);

        DB::table('sintomas')->insert([
            'nombre' => 'Tos seca',
            'descripcion' => Str::random(70),
        ]);

        DB::table('sintomas')->insert([
            'nombre' => 'Tos',
            'descripcion' => Str::random(70),
        ]);

        DB::table('sintomas')->insert([
            'nombre' => 'Flema',
            'descripcion' => Str::random(70),
        ]);

        DB::table('sintomas')->insert([
            'nombre' => 'Presion Arterial Alta',
            'descripcion' => Str::random(70),
        ]);

        DB::table('sintomas')->insert([
            'nombre' => 'Escalofrio',
            'descripcion' => Str::random(70),
        ]);
    }
}
