<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BeneficiarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('beneficiarios')->insert([
            'name' => Str::random(10),
            'lastname' => Str::random(10),
            'idCode' =>rand(),
            'idType'=>'CC',
        ]);

        DB::table('beneficiarios')->insert([
            'name' => Str::random(10),
            'lastname' => Str::random(10),
            'idCode' =>rand(),
            'idType'=>'CC',
        ]);

        DB::table('beneficiarios')->insert([
            'name' => Str::random(10),
            'lastname' => Str::random(10),
            'idCode' =>rand(),
            'idType'=>'CC',
        ]);

    }
}
