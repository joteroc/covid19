<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SolicitudBeneficiarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('solicitud_beneficiarios')->insert([
            'name' => Str::random(10),
            'lastname' => Str::random(10),
            'idCode' =>rand(),
            'idType'=>'CC',
            'email' => Str::random(10).'@covid19.com',
            'gender' => 'Masculino',
            'estado' => 'Pendiente',
            'comentario'=>Str::random(10),
        ]);

        DB::table('solicitud_beneficiarios')->insert([
            'name' => Str::random(10),
            'lastname' => Str::random(10),
            'idCode' =>rand(),
            'idType'=>'CC',
            'email' => Str::random(10).'@covid19.com',
            'gender' => 'Rarito',
            'estado' => 'Rechazado',
            'comentario'=>'Su solicitud fue rechazada, usted es multimillonario',
        ]);

        DB::table('solicitud_beneficiarios')->insert([
            'name' => Str::random(10),
            'lastname' => Str::random(10),
            'idCode' =>rand(),
            'idType'=>'CC',
            'email' => Str::random(10).'@covid19.com',
            'gender' => 'Masculino',
            'estado' => 'Pendiente',
            'comentario'=>Str::random(10),
        ]);

    }
}
