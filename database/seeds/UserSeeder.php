<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jeifer Alexander',
            'lastname' => 'Oterto Carvajal',
            'idCode' =>'1059984759',
            'idType'=>'CC',
            'email' => 'joteroc@covid19.com',
            'gender' => 'Masculino',
            'password' => Hash::make('123456789'),
            'rol'=>'admin',
        ]);

        DB::table('users')->insert([
            'name' => 'Jose Eduardo',
            'lastname' => 'Cardona Idarraga',
            'idCode' =>'105998460',
            'idType'=>'CE',
            'email' => 'jcardonai@covid19.com',
            'gender' => 'Masculino',
            'rol'=>'user',
            'password' => Hash::make('123456789'),
        ]);


    }
}
