<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class EnfermedadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enfermedads')->insert([
            'nombre' => 'Covid-19',
            'descripcion' => Str::random(70),
        ]);

        DB::table('enfermedads')->insert([
            'nombre' => 'SARS',
            'descripcion' => Str::random(70),
        ]);

        DB::table('enfermedads')->insert([
            'nombre' => 'Influenza',
            'descripcion' => Str::random(70),
        ]);

        DB::table('enfermedads')->insert([
            'nombre' => 'SIDA',
            'descripcion' => Str::random(70),
        ]);


    }
}
