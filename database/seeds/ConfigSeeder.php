<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([
            'nombre' => 'limite_solicitudes',
            'valor' => '5',
            'descripcion' => Str::random(70),
        ]);

        DB::table('configs')->insert([
            'nombre' => 'estado_usuario_1',
            'valor' => 'Activo',
            'descripcion' => Str::random(70),
        ]);

        DB::table('configs')->insert([
            'nombre' => 'estado_usuario_0',
            'valor' => 'Inactivo',
            'descripcion' => Str::random(70),
        ]);

        DB::table('configs')->insert([
            'nombre' => 'estado_peticion_0',
            'valor' => 'Pendiente',
            'descripcion' => Str::random(70),
        ]);

        DB::table('configs')->insert([
            'nombre' => 'estado_peticion_1',
            'valor' => 'Finalizada',
            'descripcion' => Str::random(70),
        ]);
    }
}
