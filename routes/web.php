<?php

use App\Http\Controllers\ControllerMail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('inicio');

Route::get('conocenos', function () {
    return view('conocenos');
})->name('conocenos');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('reportes')->group(function () {
    Route::get('principal', 'ReportesController@principal')->name('reportes.principal');
    Route::get('usuarios', 'ReportesController@usuarios')->name('reportes.usuarios');
    Route::get('graficos.usuarios', 'ReportesController@graficos_usuarios')->name('reportes.graficos.usuarios');
    Route::get('beneficiarios','ReportesController@beneficiarios')->name('reportes.beneficiarios');
    Route::get('enfermedades','ReportesController@enfermedades')->name('reportes.enfermedades');
    Route::get('roles','ReportesController@roles')->name('reportes.roles');
    Route::get('configs','ReportesController@configs')->name('reportes.configs');
    Route::get('solicitudes','ReportesController@solicitudes_beneficiarios')->name('reportes.solicitudes');
    Route::get('graficos.solicitudes', 'ReportesController@graficos_solicitudes')->name('reportes.graficos.solicitudes');
});

// ruta para el formulario de creacion del correo
Route::get('/contactanos', 'ControllerMail@index')->name('contactanos');
// ruta al enviar correo
Route::post('/send', 'ControllerMail@send')->name('send');


Route::prefix('enfermedades')->group(function () {
    Route::get('todas', 'EnfermedadController@index')->name('enfermedades');
    Route::post('crear','EnfermedadController@store')->name('enfermedades.crear');
});

Route::prefix('diagnosticos')->group(function () {
    Route::get('todas', 'DiagnosticoController@index')->name('diagnosticos');
    Route::post('crear','DiagnosticoController@store')->name('diagnosticos.crear');
});

Route::prefix('beneficiarios')->group(function () {
    Route::get('todas', 'BeneficiarioController@index')->name('beneficiarios');
    Route::post('crear','BeneficiarioController@store')->name('beneficiarios.crear');
});

Route::prefix('roles')->group(function () {
    Route::get('todas', 'RolController@index')->name('roles');
    Route::post('crear','RolController@store')->name('roles.crear');
});

Route::prefix('usuarios')->group(function () {
    Route::get('todas', 'UserController@index')->name('usuarios');
    Route::get('perfil/{id}','UserController@show')->name('perfil');
    Route::post('modificar','UserController@update')->name('modificar.perfil');
    Route::post('modificar_password','UserController@updatePassword')->name('modificar.password');
});

Route::prefix('solicitudes')->group(function () {
    Route::get('todas', 'SolicitudBeneficiarioController@index')->name('solicitudes');
    Route::post('crear','SolicitudBeneficiarioController@store')->name('crear.solicitud');
    Route::get('misolicitud/{idCode}','SolicitudBeneficiarioController@showByIdBeneficiario')->name('misolicitud');
    Route::post('decidir','SolicitudBeneficiarioController@decidir')->name('solicitud.decidir');
});

Route::prefix('configuraciones')->group(function () {
    Route::get('todas', 'ConfigController@index')->name('configuraciones');
    Route::post('crear','ConfigController@store')->name('configuraciones.crear');
    Route::get('modificar/{idCode}','ConfigController@update')->name('modificar.configuracion');
});

Route::prefix('sintomas')->group(function () {
    Route::get('todas', 'SintomaController@index')->name('sintomas');
    Route::post('crear','SintomaController@store')->name('sintomas.crear');
    Route::get('modificar/{idCode}','SintomaController@update')->name('sintomas.modificar');
});
