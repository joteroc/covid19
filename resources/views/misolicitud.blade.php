@extends('plantillas.main')
@section('contenido')
@if(session('mensaje'))
                    <div class="alert alert-success alert-dismissible fade show">
                       {{session('mensaje')}}
                    </div>
     @endif
     @if(session('mensaje_error'))
                    <div class="alert alert-danger alert-dismissible fade show">
                       {{session('mensaje_error')}}
                    </div>
     @endif
    @component('plantillas.cards')
        @section('card-tittle')
            Listado de Solicitudes
        @endsection
        @section('card-body')
        @if($solicitudes!=null)
          <div>
            <table class="table table-striped table-responsive-xl table-responsive-lg">
                <thead>
                <tr>
                    <th scope="col">ID Solicitud</th>
                    <th scope="col">ID Solicitante</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Genero</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Estado Solicitud</th>
                    <th scope="col">Comentario</th>

                </tr>
                </thead>
                <tbody>
                    @foreach ($solicitudes as $solicitud)
                        <tr>
                        <th scope="row">{{$solicitud->id}}</th>
                            <td>{{Auth::user()->idType}} {{Auth::user()->idCode}}</td>
                            <td>{{Auth::user()->name}}</td>
                            <td>{{Auth::user()->lastname}}</td>
                            <td>{{Auth::user()->gender}}</td>
                            <td>{{Auth::user()->email}}</td>
                            <td>{{$solicitud->estado}}</td>
                            <td>{{$solicitud->comentario}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
        <h3> No hay Datos </h3>
        @endif
        @endsection
        @section('card-footer')
        @endsection
    @endcomponent


@endsection


