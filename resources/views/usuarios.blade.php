@extends('plantillas.main')
@section('contenido')
    @component('plantillas.cards')
        @section('card-tittle')
            Usuarios Registrados
        @endsection
        @section('card-body')

          <div>
            <table class="table table-striped table-responsive-xl table-responsive-lg">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Genero</th>
                    <th scope="col">Correo</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($usuarios as $usuarios)
                        <tr>
                        <th scope="row">{{$usuarios->idType}} {{$usuarios->idCode}}</th>
                            <td>{{$usuarios->name}}</td>
                            <td>{{$usuarios->lastname}}</td>
                            <td>{{$usuarios->gender}}</td>
                            <td>{{$usuarios->email}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endsection
        @section('card-footer')
        @endsection
    @endcomponent


@endsection


