@extends('plantillas.main')
@section('contenido')
<div class="container my-5">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Mi informacion</div>

                <div class="card-body">
                    @if(session('mensaje'))
                    <div class="alert alert-success alert-dismissible fade show">
                       {{session('mensaje')}}
                    </div>
                    @endif
                    @if(session('mensaje_error'))
                                    <div class="alert alert-danger alert-dismissible fade show">
                                    {{session('mensaje_error')}}
                                    </div>
                    @endif
                    <form method="POST" action="{{ route('modificar.perfil') }}">
                        @csrf
                        <input type="hidden" class="form-control invisible" name="id" value="{{$usuario->id}}">
                        <input type="hidden" class="form-control invisible" name="rol" value="{{$usuario->rol}}">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$usuario->name}}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Lastname') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{$usuario->lastname}}" required autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>
                            <div class="col-md-6">
                                <select id="gender" class="custom-select form-control @error('gender') is-invalid @enderror" name="gender" value="{{$usuario->gender}}" required autocomplete="gender" autofocus>
                                    <option selected>{{$usuario->gender}}</option>
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                    <option value="Rarito">Rarito</option>
                                </select>
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="idType" class="col-md-4 col-form-label text-md-right">{{ __('Tipo ID') }}</label>

                            <div class="col-md-6">
                                <select id="idType" class="custom-select form-control @error('idType') is-invalid @enderror" name="idType" value="{{$usuario->idType}}" required autocomplete="idType" autofocus>
                                    <option selected>{{$usuario->idType}}</option>
                                    <option value="CC">CC</option>
                                    <option value="TI">TI</option>
                                    <option value="CE">CE</option>
                                    <option value="PP">PP</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="idCode" class="col-md-4 col-form-label text-md-right">{{ __('Numero ID') }}</label>

                            <div class="col-md-6">
                                <input id="idCode" type="text" class="form-control @error('idCode') is-invalid @enderror" name="idCode" readonly value="{{$usuario->idCode}}" required autocomplete="idCode" autofocus>

                                @error('idCode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" readonly value="{{$usuario->email}}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-5">
                                <button type="submit" class="btn btn-primary">
                                    Modificar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                        <div class="container my-4">
                            <form method="POST" action="{{ route('crear.solicitud') }}">
                                @csrf
                                    <input type="hidden" class="form-control invisible" name="name" value="{{$usuario->name}}">
                                    <input type="hidden" class="form-control invisible" name="lastname" value="{{$usuario->lastname}}">
                                    <input type="hidden" class="form-control invisible" name="gender" value="{{$usuario->gender}}">
                                    <input type="hidden" class="form-control invisible" name="idCode" value="{{$usuario->idCode}}">
                                    <input type="hidden" class="form-control invisible" name="idType" value="{{$usuario->idType}}">
                                    <input type="hidden" class="form-control invisible" name="email" value="{{$usuario->email}}">
                                    <input type="hidden" class="form-control invisible" name="estado" value="Pendiente">
                                    <input type="hidden" class="form-control invisible" name="comentario" value="Solicito revisar mi datos para aplicar a beneficios">

                                    @auth
                                        @if(Auth::user()->rol != 'admin')
                                        <button type="submit" class="btn btn-danger btn-block @if(session('mensaje')) disabled @endif">
                                            Solicitar Beneficio
                                        </button>
                                        @endif
                                    @endauth

                            </form>
                        </div>
                </div>

            </div>

        </div>

    </div>

    <div class="row justify-content-center mb-5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Cambiar Contraseña</div>
                <div class="card-body">
                    <div class="container">
                            <form method="POST" action="{{ route('modificar.password') }}">
                                @csrf


                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <input type="hidden" class="form-control invisible" name="id" value="{{$usuario->id}}">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                    <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-5">
                                        <button type="submit" class="btn btn-primary">
                                            Cambiar Contraseña
                                        </button>
                                    </div>
                            </form>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
