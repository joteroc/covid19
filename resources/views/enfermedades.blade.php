@extends('plantillas.main')
@section('contenido')
@component('plantillas.modals')@endcomponent
    @component('plantillas.cards')
        @section('card-tittle')
            Enfermedades
        @endsection
        @section('card-body')

        <div class="row">
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.fi.edu%2Fsites%2Fdefault%2Ffiles%2Fimages%2Fblog%2F1580941598923.jpeg&f=1&nofb=1" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">SARS</h5>
                      <button type="button" class="btn btn-outline-info" data-toggle="modal"  data-target="#sars">Info</button>
                    </div>
                  </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="https://img.theepochtimes.com/assets/uploads/2020/02/29/coronavirus-faces-700x420.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Influenza</h5>
                      <button type="button" class="btn btn-outline-info" data-toggle="modal"  data-target="#influenza">Info</button>
                    </div>
                  </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcultura.biografieonline.it%2Fwp-content%2Fuploads%2F2020%2F03%2Fcoronavirus_-SARS-CoV-2_COVID-19.jpg&f=1&nofb=1" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Covid-19</h5>
                      <button type="button" class="btn btn-outline-info" data-toggle="modal"  data-target="#info_covid19">Info</button>
                    </div>
                  </div>
            </div>
          </div>

          @if($enfermedades!=null)
          <div class="my-5">
            <table class="table table-striped table-responsive-xl table-responsive-lg">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripcion</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($enfermedades as $enfermedad)
                        <tr>
                        <th scope="row">{{$enfermedad->id}}</th>
                            <td>{{$enfermedad->nombre}}</td>
                            <td>{{$enfermedad->descripcion}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif

        <div class="text-left modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <form method="POST" action="{{ route('enfermedades.crear') }}">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="titulo">Crear Enfermedad</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                        @csrf
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nombre:</label>
                      <input name="nombre" type="text" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Descripcion:</label>
                      <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </div>
            </div>
        </form>
          </div>
        @endsection
        @section('card-footer')
        @auth
        @if(Auth::user()->rol == 'admin')
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#crear">Crear</button>
        @endif
        @endauth
        @endsection
    @endcomponent


@endsection


