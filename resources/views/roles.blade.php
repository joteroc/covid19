@extends('plantillas.main')
@section('contenido')
    @component('plantillas.cards')
        @section('card-tittle')
            Listado de Roles
        @endsection
        @section('card-body')
        @if($roles!=null)
            <div>
                <table class="table table-striped table-responsive-xl table-responsive-lg">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripcion</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $rol)
                            <tr>
                            <th scope="row">{{$rol->id}} {{$rol->idCode}}</th>
                                <td>{{$rol->nombre}}</td>
                                <td>{{$rol->descripcion}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
        <h3>-- No hay Datos --</h3>
        @endif

        <div class="text-left modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <form method="POST" action="{{ route('roles.crear') }}">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="titulo">Crear Rol</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                        @csrf
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nombre:</label>
                      <input name="nombre" type="text" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Descripcion:</label>
                      <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </div>
            </div>
        </form>
          </div>
        @endsection
        @section('card-footer')
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#crear">Crear</button>
        @endsection
    @endcomponent


@endsection


