@extends('plantillas.main')
@section('contenido')
    @component('plantillas.cards')
        @section('card-tittle')
            Listado de Solicitudes
        @endsection
        @section('card-body')
        @if($solicitudes!=null)
          <div>
            <table class="table table-striped table-responsive-xl table-responsive-lg">
                <thead>
                <tr>
                    <th scope="col">ID Solicitud</th>
                    <th scope="col">ID Solicitante</th>
                    <th scope="col">Solicitante</th>
                    <th scope="col">Genero</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Estado Solicitud</th>
                    <th scope="col">Comentario</th>
                    <th scope="col">Accion</th>

                </tr>
                </thead>
                <tbody>
                    @foreach ($solicitudes as $solicitud)
                        <tr>
                        <th scope="row">{{$solicitud->id}}</th>
                            <td>{{$solicitud->idType}} {{$solicitud->idCode}}</td>
                            <td>{{$solicitud->name }} {{$solicitud->lastname}}</td>
                            <td>{{$solicitud->gender}}</td>
                            <td>{{$solicitud->email}}</td>
                            <td>{{$solicitud->estado}}</td>
                            <td>{{$solicitud->comentario}}</td>
                            <td>
                                @if ($solicitud->estado=='Pendiente')
                                    <form method="POST" action="{{ route('solicitud.decidir') }}">
                                        @csrf
                                        <input type="hidden" class="form-control invisible" name="id" value="{{$solicitud->id}}">
                                        <input type="hidden" class="form-control invisible" name="idCode" value="{{$solicitud->idCode}}">
                                        <input type="hidden" class="form-control invisible" name="accion" value="aprobar">
                                        <button type="submit" class="btn btn-success">
                                            Aprobar
                                        </button>
                                    </form>

                                    <form method="POST" action="{{ route('solicitud.decidir') }}">
                                        @csrf
                                        <input type="hidden" class="form-control invisible" name="id" value="{{$solicitud->id}}">
                                        <input type="hidden" class="form-control invisible" name="idCode" value="{{$solicitud->idCode}}">
                                        <input type="hidden" class="form-control invisible" name="accion" value="denegar">
                                        <button type="submit" class="btn btn-danger my-1">
                                            Denegar
                                        </button>
                                    </form>
                            @else
                                <h3><span class="badge badge-secondary">Procesada</span></h3>
                            @endif
                        </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
        <h3> No hay Datos </h3>
        @endif
        @endsection
        @section('card-footer')
        @endsection
    @endcomponent


@endsection


