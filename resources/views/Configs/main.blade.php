@extends('plantillas.main')
@section('contenido')
@component('plantillas.modals')@endcomponent
    @component('plantillas.cards')
        @section('card-tittle')
            Configuraciones
        @endsection
        @section('card-body')
          @if($configuraciones!=null)
          <div class="my-5">
            <table class="table table-striped table-responsive-xl table-responsive-lg">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Descripcion</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($configuraciones as $configuracion)
                        <tr>
                        <th scope="row">{{$configuracion->id}}</th>
                            <td>{{$configuracion->nombre}}</td>
                            <td>{{$configuracion->valor}}</td>
                            <td>{{$configuracion->descripcion}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif

        <div class="text-left modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <form method="POST" action="{{ route('configuraciones.crear') }}">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="titulo">Crear Parametro de Sistema</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                        @csrf
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nombre:</label>
                      <input name="nombre" type="text" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Valor:</label>
                        <input name="valor" type="text" class="form-control" id="valor">
                      </div>
                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Descripcion:</label>
                      <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </div>
            </div>
        </form>
          </div>

        @endsection
        @section('card-footer')
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#crear">Crear</button>
        @endsection
    @endcomponent

@endsection


