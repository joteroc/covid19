@extends('plantillas.main')
@section('contenido')
    @component('plantillas.cards')
        @section('card-tittle')
            Reporte Solicitudes
        @endsection
        @section('card-body')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    var analytics = <?php echo $solicitud ?>;

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(
          analytics
        );

        var options = {
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>


    <div id="piechart" style="width: 900px; height: 500px; text-center"></div>
    @endsection
        @section('card-footer')
        @endsection
    @endcomponent
@endsection
