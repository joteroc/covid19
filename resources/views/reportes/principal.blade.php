@extends('plantillas.main')
@section('contenido')
    @component('plantillas.cards')
        @section('card-tittle')
            Generacion de Reportes
        @endsection
        @section('card-body')
            <div class="container my-5">
                <div class="row">
                <div class="col-sm">
                    <a type="button" class="btn btn-info btn-block btn-lg" href="{{route('reportes.usuarios')}}">Usuarios <span class="sr-only">(current)</span></a>
                    <a type="button" class="btn btn-info btn-block btn-lg" href="{{route('reportes.graficos.usuarios')}}">Grafico Usuarios <span class="sr-only">(current)</span></a>
                </div>
                <div class="col-sm">
                    <a type="button" class="btn btn-info btn-block btn-lg" href="{{route('reportes.configs')}}">Configuraciones <span class="sr-only">(current)</span></a>
                </div>
                <div class="col-sm">
                    <a type="button" class="btn btn-info btn-block btn-lg" href="{{route('reportes.enfermedades')}}">Enfermedades <span class="sr-only">(current)</span></a>
                </div>
                </div>
            </div>
            <div class="container my-5">
                <div class="row">
                <div class="col-sm">
                    <a type="button" class="btn btn-info btn-block btn-lg" href="{{route('reportes.roles')}}">Roles <span class="sr-only">(current)</span></a>
                </div>
                <div class="col-sm">
                    <a type="button" class="btn btn-info btn-block btn-lg" href="{{route('reportes.beneficiarios')}}">Beneficiarios <span class="sr-only">(current)</span></a>
                </div>
                <div class="col-sm">
                    <a type="button" class="btn btn-info btn-block btn-lg" href="{{route('reportes.solicitudes')}}">Solicitudes <span class="sr-only">(current)</span></a>
                    <a type="button" class="btn btn-info btn-block btn-lg" href="{{route('reportes.graficos.solicitudes')}}">Grafico Solicitudes <span class="sr-only">(current)</span></a>

                </div>
                </div>
            </div>
        @endsection
        @section('card-footer')
        @endsection
    @endcomponent


@endsection


