@extends('plantillas.main')
@section('contenido')
    @component('plantillas.cards')
        @section('card-tittle')
            Auto Valoracion
        @endsection
        @section('card-body')
        @if($sintomas!=null)
        <div>
            <table class="table table-striped table-responsive-xl table-responsive-lg">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Seleccionar</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($sintomas as $sintoma)
                        <tr>
                        <th scope="row">{{$sintoma->id}}</th>
                            <td>{{$sintoma->nombre}}</td>
                            <td>{{$sintoma->descripcion}}</td>
                            <td><div class="form-check">
                                <input name="sintoma" value="{{$sintoma->id}}" type="checkbox" class="form-check-input" id="{{$sintoma->id}}">
                              </div></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <a type="button" class="btn btn-info btn-lg" href="#">Enviar <span class="sr-only">(current)</span></a>
        </div>
    @else
    <h3>-- No hay Datos --</h3>
    @endif
        @endsection
        @section('card-footer')

        @endsection
    @endcomponent


@endsection


