<div class="card text-center my-5">
        <h4 class="card-header">@yield('card-tittle')</h4>
    <div class="card-body">
    @if(session('mensaje'))
                    <div class="alert alert-success alert-dismissible fade show">
                       {{session('mensaje')}}
                    </div>
     @endif
     @if(session('mensaje_error'))
                    <div class="alert alert-danger alert-dismissible fade show">
                       {{session('mensaje_error')}}
                    </div>
     @endif
      @yield('card-body')
    </div>
    <div class="card-footer text-muted">
      @yield('card-footer')
    </div>
  </div>
