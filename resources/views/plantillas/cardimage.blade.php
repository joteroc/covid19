<div class="card" style="width: 18rem;">
    <img src="@yield('cardimage-image')" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">@yield('cardimage-tittle')</h5>
      @yield('cardimage-content')
    </div>
  </div>
