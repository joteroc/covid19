<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{route('inicio')}}">Maestria USC-2020A</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="{{route('enfermedades')}}">Enfermedades <span class="sr-only">(current)</span></a>
            </li>
        @auth
        <li class="nav-item active">
                <a class="nav-link" href="{{route('beneficiarios')}}">Beneficiarios <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="{{route('home')}}">Diagnostico <span class="sr-only">(current)</span></a>
    </li>

        @if(Auth::user()->rol == 'admin')
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Administracion
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{route('usuarios')}}">Usuarios <span class="sr-only">(current)</span></a>
                <a class="dropdown-item" href="{{route('roles')}}">Roles <span class="sr-only">(current)</span></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{route('sintomas')}}">Sintomas <span class="sr-only">(current)</span></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{route('solicitudes')}}">Solicitudes <span class="sr-only">(current)</span></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{route('reportes.principal')}}">Reportes <span class="sr-only">(current)</span></a>

            </div>
            </li>
        @endif

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Conocenos
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('conocenos')}}">Nosotros <span class="sr-only">(current)</span></a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{route('contactanos')}}">Contactanos <span class="sr-only">(current)</span></a>
          </div>
        </li>
        @endauth
        <li class="nav-item active">

        </li>
      </ul>

      <!-- Right Side Of Navbar -->
      <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} {{ Auth::user()->lastname }}<span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{route('perfil',Auth::user()->id)}}">Mis Datos <span class="sr-only">(current)</span></a>
                    <a class="dropdown-item" href="{{route('misolicitud',Auth::user()->idCode)}}">Mis Solicitudes<span class="sr-only">(current)</span></a>
                    <a class="dropdown-item" href="#">Mis Diagnosticos<span class="sr-only">(current)</span></a>
                    @if(Auth::user()->rol == 'admin')
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('configuraciones')}}">Configuraciones<span class="sr-only">(current)</span></a>
                    @endif
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>

      {{-- comment

      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form> --}}
    </div>
  </nav>
