<!-- Modal Info Covid-19 -id= info_covid19-->
<div class="modal fade" id="info_covid19" tabindex="-1" role="dialog" aria-labelledby="info_covid19Title" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="info_covid19Title">COVID-19</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            También conocida como enfermedad por coronavirus o, incorrectamente, como neumonía por coronavirus,​ es una enfermedad infecciosa causada por el virus SARS-CoV-2.
            <p>Se detectó por primera vez en la ciudad china de Wuhan (provincia de Hubei) en diciembre de 2019.11​12​ Habiendo llegado a más de 100 territorios, el 11 de marzo de 2020 la Organización Mundial de la Salud la declaró pandemia.</p>
            <p>Produce síntomas similares a los de la gripe, entre los que se incluyen fiebre, tos seca, disnea, mialgia y fatiga. </p>
            <p>15​16​ En casos graves se caracteriza por producir neumonía, síndrome de dificultad respiratoria aguda, sepsis y choque séptico que conduce a alrededor del 3 % de los infectados a la muerte.</p>
            <p>No existe tratamiento específico; las medidas terapéuticas principales consisten en aliviar los síntomas y mantener las funciones vitales</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal Info SARS-id= sars-->
<div class="modal fade" id="sars" tabindex="-1" role="dialog" aria-labelledby="sarsTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="sarsTitle">SARS</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            Ocasionalmente llamado SARS-CoV-1 para distinguirlo del SARS-CoV-2, es una especie de coronavirus que causa el síndrome respiratorio agudo grave (SARS, por sus siglas en inglés) que surgió en 2003 en los países del sudeste asiático. Emergió en noviembre de 2002 en la provincia china de Cantón. Desde su surgimiento a agosto de 2003, el virus había infectado 8422 personas en una treintena de países y causado 916 decesos.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Info Influenza id=influenza-->
<div class="modal fade" id="influenza" tabindex="-1" role="dialog" aria-labelledby="influenzaTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="influenzaTitle">Influenza</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            En los seres humanos puede afectar las vías respiratorias, esto es, la nariz, la garganta, los bronquios y, con poca frecuencia, los pulmones; sin embargo, también puede afectar al corazón, el cerebro o los músculos. La gripe suele curarse espontáneamente en algunos días, pero en algunos casos puede agravarse debido a complicaciones que pueden resultar fatales, especialmente en niños pequeños, en mujeres embarazadas, en adultos mayores o en personas con el estado inmunitario alterado.         </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
