@extends('plantillas.main')
@section('contenido')
    @component('plantillas.cards')

        @section('card-tittle')
        Nosotros
        @endsection

        @section('card-body')
        <div class="row">
            <div class="col-md-6">
                Jeifer Otero
            </div>
            <div class="col-md-6">
                Eduardo Cardona
            </div>
          </div>
        @endsection

        @section('card-footer')
        @endsection
    @endcomponent
@endsection
