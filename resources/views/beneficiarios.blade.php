@extends('plantillas.main')
@section('contenido')
    @component('plantillas.cards')
        @section('card-tittle')
            Listado Total Beneficiarios
        @endsection
        @section('card-body')
        @if($beneficiarios!=null)
          <div>
            <table class="table table-striped table-responsive-xl table-responsive-lg">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($beneficiarios as $beneficiario)
                        <tr>
                        <th scope="row">{{$beneficiario->idType}} {{$beneficiario->idCode}}</th>
                            <td>{{$beneficiario->name}}</td>
                            <td>{{$beneficiario->lastname}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
        <h3> No hay Datos </h3>
        @endif
        @endsection
        @section('card-footer')
        @endsection
    @endcomponent


@endsection


